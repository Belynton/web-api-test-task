﻿namespace MyApplication.Exceptions;

public class BadRequest : Exception
{
    public BadRequest(string message) : base(message)
    {
    }
}