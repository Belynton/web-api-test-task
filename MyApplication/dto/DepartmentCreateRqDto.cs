﻿namespace MyApplication.dto;

public class DepartmentCreateRqDto
{
    public string Name { get; set; }
    public string Phone { get; set; }
}