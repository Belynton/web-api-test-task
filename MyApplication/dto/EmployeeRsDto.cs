﻿namespace MyApplication.dto;

public class EmployeeRsDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Surname { get; set; }
    public string? Phone { get; set; }
    public int CompanyId { get; set; }
    public PassportRsDto Passport { get; set; }
    public DepartmentRsDto Department { get; set; }
}