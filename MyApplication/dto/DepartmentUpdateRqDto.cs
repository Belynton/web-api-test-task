﻿namespace MyApplication.dto;

public class DepartmentUpdateRqDto
{
    public string? Name { get; set; }

    public string? Phone { get; set; }
}