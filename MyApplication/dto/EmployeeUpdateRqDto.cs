﻿namespace MyApplication.dto;

public class EmployeeUpdateRqDto
{
    public string? Name { get; set; } 
    public string? Surname { get; set; }
    public string? Phone { get; set; }
    public int CompanyId { get; set; }
    public PassportUpdateRqDto? Passport { get; set; } 
    public DepartmentUpdateRqDto? Department { get; set; }
}