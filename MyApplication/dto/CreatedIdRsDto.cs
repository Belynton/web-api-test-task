﻿namespace MyApplication.dto;

public class CreatedIdRsDto
{
    public CreatedIdRsDto(int id)
    {
        Id = id;
    }

    public int Id { get; }
}