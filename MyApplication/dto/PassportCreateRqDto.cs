﻿namespace MyApplication.dto;

public class PassportCreateRqDto
{
    public string Type { get; set; }

    public string Number { get; set; }
}