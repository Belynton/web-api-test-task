﻿namespace MyApplication.dto;

public class PassportUpdateRqDto
{
    public string? Type { get; set; }

    public string? Number { get; set; }
}