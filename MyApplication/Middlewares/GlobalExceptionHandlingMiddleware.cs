﻿using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using MyApplication.Exceptions;

namespace MyApplication.Middlewares;

public class GlobalExceptionHandlingMiddleware : IMiddleware
{

    private readonly ILogger<GlobalExceptionHandlingMiddleware> _logger;

    public GlobalExceptionHandlingMiddleware(ILogger<GlobalExceptionHandlingMiddleware> logger)
    {
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next.Invoke(context);
        }
        catch (NotFoundException exc)
        {
            await WriteAsync(context, exc.Message, HttpStatusCode.NotFound);
        }
        catch (BadRequest exc)
        {
            await WriteAsync(context, exc.Message, HttpStatusCode.BadRequest);
        }
        catch (Exception exc)
        {
            _logger.LogError(exc, exc.Message);
            await WriteAsync(context, "Server error", HttpStatusCode.InternalServerError);
        }
    }
    
    private async Task WriteAsync(HttpContext context, string message, HttpStatusCode statusCode)
    {
        context.Response.StatusCode = (int)statusCode;
        ProblemDetails problemDetails = new()
        {
            Status = (int)statusCode,
            Title = message
        };
        var json = JsonSerializer.Serialize(problemDetails);
        context.Response.ContentType = "application/json";
        await context.Response.WriteAsync(json);
    }
    
}