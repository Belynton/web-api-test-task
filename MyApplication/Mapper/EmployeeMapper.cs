﻿using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Repository;

namespace MyApplication.Mapper;

public class EmployeeMapper : IEmployeeMapper
{
    private readonly IDepartmentRepository _departmentRepository;
    private readonly IPassportRepository _passportRepository;

    public EmployeeMapper(IPassportRepository passportRepository, IDepartmentRepository departmentRepository)
    {
        _passportRepository = passportRepository;
        _departmentRepository = departmentRepository;
    }

    public EmployeeRsDto ToRsDto(Employee employee)
    {
        var result = new EmployeeRsDto
        {
            Id = employee.Id,
            Name = employee.Name,
            Surname = employee.Surname,
            Phone = employee.Phone,
            CompanyId = employee.CompanyId,
            Department = _departmentRepository.FindById(employee.DepartmentId).Result,
            Passport = _passportRepository.FindById(employee.PassportId).Result
        };
        return result;
    }
}