﻿using System.Data;
using MyApplication.dto;

namespace MyApplication.Repository;

public interface IPassportRepository
{
    Task<PassportRsDto> FindById(int id);

    Task<CreatedIdRsDto> Create(PassportCreateRqDto passportRqDto, IDbConnection connection);

    Task Delete(int id, IDbConnection connection);
    
    Task Update(int id, PassportUpdateRqDto passportRqDto, IDbConnection connection);
}