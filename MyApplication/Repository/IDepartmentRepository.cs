﻿using System.Data;
using MyApplication.dto;

namespace MyApplication.Repository;

public interface IDepartmentRepository
{
    Task<DepartmentRsDto> FindById(int id);

    Task<CreatedIdRsDto> Create(DepartmentCreateRqDto departmentCreate, IDbConnection connection);

    Task Delete(int id, IDbConnection connection);

    Task Update(int id, DepartmentUpdateRqDto departmentCreateRqDto, IDbConnection connection);
}