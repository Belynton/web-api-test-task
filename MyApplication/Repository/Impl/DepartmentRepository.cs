﻿using System.Data;
using System.Text.RegularExpressions;
using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Exceptions;

namespace MyApplication.Repository.Impl;

public class DepartmentRepository : IDepartmentRepository
{
    private readonly DapperContext _context;

    public DepartmentRepository(DapperContext context)
    {
        _context = context;
    }

    public async Task<CreatedIdRsDto> Create(DepartmentCreateRqDto departmentRqDto, IDbConnection connection)
    {
        ThrowIfPhoneNotValid(departmentRqDto.Phone);
        var query = $"INSERT INTO department " +
                    $"(\"name\", phone) " +
                    $"VALUES(@Name, @Phone) " +
                    $"RETURNING id";

        var returnedId = await connection.ExecuteScalarAsync<int>(query, departmentRqDto);
        return new CreatedIdRsDto(returnedId);
    }

    public async Task<DepartmentRsDto> FindById(int id)
    {
        var query = $"SELECT id, name, phone FROM department WHERE id={id}";
        using var connection = _context.CreateConnection();
        var department = await connection.QueryFirstAsync<DepartmentRsDto>(query);
        if (department == null)
        {
            throw new NotFoundException($"Department with id = {id} not found!");
        }

        return department;
    }

    public async Task Delete(int id, IDbConnection connection)
    {
        var query = $"DELETE FROM department WHERE id={id}";
        await connection.ExecuteAsync(query);
    }

    public async Task Update(int id, DepartmentUpdateRqDto departmentRqDto, IDbConnection connection)
    {
        var departmentRsDto = await FindById(id);
        CopyNotNullFields(departmentRsDto, departmentRqDto);
        ThrowIfPhoneNotValid(departmentRqDto.Phone!);
        var query = $"UPDATE department SET \"name\" = @Name, phone = @Phone " +
                    $"WHERE id = {id}";
        await connection.ExecuteAsync(query, departmentRqDto);
    }

    private DepartmentUpdateRqDto CopyNotNullFields(DepartmentRsDto departmentRsDto, DepartmentUpdateRqDto departmentRqDto)
    {
        departmentRqDto.Phone ??= departmentRsDto.Phone;

        departmentRqDto.Name ??= departmentRsDto.Name;

        return departmentRqDto;
    }

    private void ThrowIfPhoneNotValid(string number)
    {
        var phoneTemplate = new Regex(@"\+[1-9]{1}[0-9]{10}$");
        if (!phoneTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect phone number.");
        }
    }
}