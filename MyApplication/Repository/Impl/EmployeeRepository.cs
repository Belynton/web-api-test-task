﻿using System.Text.RegularExpressions;
using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Exceptions;
using MyApplication.Mapper;

namespace MyApplication.Repository.Impl;

public class EmployeeRepository : IEmployeeRepository
{
    private readonly DapperContext _context;
    private readonly IEmployeeMapper _employeeMapper;
    private readonly IDepartmentRepository _departmentRepository;
    private readonly IPassportRepository _passportRepository;

    public EmployeeRepository(DapperContext context, IEmployeeMapper employeeMapper,
        IDepartmentRepository departmentRepository, IPassportRepository passportRepository)
    {
        _context = context;
        _employeeMapper = employeeMapper;
        _departmentRepository = departmentRepository;
        _passportRepository = passportRepository;
    }

    public async Task<CreatedIdRsDto> Create(EmployeeCreateRqDto employeeRqDto)
    {
        ThrowIfPhoneNotValid(employeeRqDto.Phone);
        if (employeeRqDto.CompanyId == 0)
        {
            throw new BadRequest("Incorrect company id value.");
        }

        int returnedId;
        using var connection = _context.CreateConnection();
        connection.Open();
        using (var transaction = connection.BeginTransaction())
        {
            var department = await _departmentRepository.Create(employeeRqDto.Department, connection);
            var passport = await _passportRepository.Create(employeeRqDto.Passport, connection);
            var query = $"INSERT INTO employee (name, surname, phone, company_id, passport_id, department_id) " +
                        $"VALUES(@Name, @Surname, @Phone, @CompanyId, {passport.Id}, {department.Id}) " +
                        $"RETURNING id";
            returnedId = await connection.ExecuteScalarAsync<int>(query, employeeRqDto);
            transaction.Commit();
        }

        return new CreatedIdRsDto(returnedId);
    }

    public async Task Delete(int id)
    {
        var founded = await FindById(id);
        var query = $"DELETE FROM employee WHERE id={id}";
        using var connection = _context.CreateConnection();
        connection.Open();
        using var transaction = connection.BeginTransaction();
        await connection.ExecuteAsync(query);
        await _departmentRepository.Delete(founded.Department.Id, connection);
        await _passportRepository.Delete(founded.Passport.Id, connection);
        transaction.Commit();
    }

    public async Task<IEnumerable<EmployeeRsDto>> FindAll()
    {
        var query = $"SELECT id, name, surname, phone, company_id AS CompanyId, " +
                    $"passport_id AS PassportId, department_id AS DepartmentId " +
                    $"FROM public.employee";
        using var connection = _context.CreateConnection();
        var employees = await connection.QueryAsync<Employee>(query);
        return employees.ToList().Select(employee => _employeeMapper.ToRsDto(employee));
    }

    public async Task<IEnumerable<EmployeeRsDto>> FindAllWhereCompanyId(int id)
    {
        var query = $"SELECT id, name, surname, phone, company_id AS CompanyId, " +
                    $"passport_id AS PassportId, department_id AS DepartmentId " +
                    $"FROM employee " +
                    $"WHERE company_id={id}";
        using var connection = _context.CreateConnection();
        var employees = await connection.QueryAsync<Employee>(query);
        return employees.ToList().Select(employee => _employeeMapper.ToRsDto(employee));
    }

    public async Task<IEnumerable<EmployeeRsDto>> FindAllWhereDepartmentName(string departmentName)
    {
        var query = $"SELECT employee.name, employee.surname, employee.phone, employee.company_id AS CompanyId, " +
                    $"employee.passport_id AS PassportId, employee.department_id AS DepartmentId, employee.id " +
                    $"FROM employee join department on employee.department_id = department.id " +
                    $"WHERE department.name='{departmentName}'";
        using var connection = _context.CreateConnection();
        var employees = await connection.QueryAsync<Employee>(query);
        return employees.ToList().Select(employee => _employeeMapper.ToRsDto(employee));
    }

    private async Task<EmployeeRsDto> FindById(int id)
    {
        var query = $"SELECT \"name\", surname, phone, company_id AS CompanyId," +
                    $" passport_id AS PassportId, department_id AS DepartmentId," +
                    $" id FROM employee WHERE id={id}";

        var connection = _context.CreateConnection();
        var employee = await connection.QueryFirstOrDefaultAsync<Employee>(query);
        if (employee == null)
        {
            throw new NotFoundException($"Employee with id = {id} not found!");
        }

        return _employeeMapper.ToRsDto(employee);
    }

    public async Task<CreatedIdRsDto> Update(int id, EmployeeUpdateRqDto employeeUpdateRqDto)
    {
        var employeeRsDto = await FindById(id);
        var copiedFieldsEmployee = CopyNotNullFields(employeeRsDto, employeeUpdateRqDto);
        ThrowIfPhoneNotValid(employeeUpdateRqDto.Phone!);

        var query = $"UPDATE employee SET \"name\" = @Name, surname = @Surname, phone = @Phone, " +
                    $"company_id = @CompanyId WHERE id = {id} RETURNING id";
        using var connection = _context.CreateConnection();
        connection.Open();
        using var transaction = connection.BeginTransaction();
        var returnedId = await connection.ExecuteScalarAsync<int>(query, copiedFieldsEmployee);
        if (employeeUpdateRqDto.Department != null)
        {
            await _departmentRepository.Update(employeeRsDto.Department.Id, employeeUpdateRqDto.Department, connection);
        }

        if (employeeUpdateRqDto.Passport != null)
        {
            await _passportRepository.Update(employeeRsDto.Passport.Id, employeeUpdateRqDto.Passport, connection);
        }

        transaction.Commit();
        return new CreatedIdRsDto(returnedId);
    }

    private EmployeeUpdateRqDto CopyNotNullFields(EmployeeRsDto employeeRsDto, EmployeeUpdateRqDto employeeUpdateRqDto)
    {
        if (string.IsNullOrEmpty(employeeUpdateRqDto.Name))
        {
            employeeUpdateRqDto.Name = employeeRsDto.Name;
        }

        if (string.IsNullOrEmpty(employeeUpdateRqDto.Surname))
        {
            employeeUpdateRqDto.Surname = employeeRsDto.Surname;
        }

        if (string.IsNullOrEmpty(employeeUpdateRqDto.Phone))
        {
            employeeUpdateRqDto.Phone = employeeRsDto.Phone;
        }

        if (employeeUpdateRqDto.CompanyId == 0)
        {
            employeeUpdateRqDto.CompanyId = employeeRsDto.CompanyId;
        }

        return employeeUpdateRqDto;
    }

    private void ThrowIfPhoneNotValid(string number)
    {
        var phoneTemplate = new Regex(@"\+[1-9]{1}[0-9]{10}$");
        if (!phoneTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect phone number.");
        }
    }
}