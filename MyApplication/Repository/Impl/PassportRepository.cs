﻿using System.Data;
using System.Text.RegularExpressions;
using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Exceptions;

namespace MyApplication.Repository.Impl;

public class PassportRepository : IPassportRepository
{
    private readonly DapperContext _context;

    public PassportRepository(DapperContext context)
    {
        _context = context;
    }

    public async Task<CreatedIdRsDto> Create(PassportCreateRqDto passportRqDto, IDbConnection connection)
    {
        ThrowIfNumberNotValid(passportRqDto.Number);
        var query = $"INSERT INTO passport (\"type\", \"number\") VALUES(@Type, @Number) RETURNING id";
        var returnedId = await connection.ExecuteScalarAsync<int>(query, passportRqDto);
        return new CreatedIdRsDto(returnedId);
    }

    public async Task Delete(int id, IDbConnection connection)
    {
        var query = $"DELETE FROM passport WHERE id={id}";
        await connection.ExecuteAsync(query);
    }

    public async Task Update(int id, PassportUpdateRqDto passportRqDto, IDbConnection connection)
    {
        var passportRsDto = await FindById(id);
        CopyNotNullFields(passportRsDto, passportRqDto);
        ThrowIfNumberNotValid(passportRqDto.Number!);
        var query = $"UPDATE passport SET number = @Number, type = @Type " +
                    $"WHERE id = {id}";
        await connection.ExecuteAsync(query, passportRqDto);
    }

    public async Task<PassportRsDto> FindById(int id)
    {
        var query = $"SELECT \"type\", \"number\", id FROM passport WHERE id={id}";
        using var connection = _context.CreateConnection();
        var passport = await connection.QueryFirstAsync<PassportRsDto>(query);
        return passport;
    }

    private PassportUpdateRqDto CopyNotNullFields(PassportRsDto passportRsDto, PassportUpdateRqDto passportRqDto)
    {
        passportRqDto.Number ??= passportRsDto.Number;

        passportRqDto.Type ??= passportRsDto.Type;

        return passportRqDto;
    }
    
    private void ThrowIfNumberNotValid(string number)
    {
        var numberTemplate = new Regex(@"[1-9]{1}[0-9]{5}$");
        if (!numberTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect passport number.");
        }
    }
}