﻿using MyApplication.dto;

namespace MyApplication.Repository;

public interface IEmployeeRepository
{
    Task<IEnumerable<EmployeeRsDto>> FindAllWhereDepartmentName(string departmentName);

    Task<IEnumerable<EmployeeRsDto>> FindAllWhereCompanyId(int id);

    Task<CreatedIdRsDto> Create(EmployeeCreateRqDto employeeRqDto);

    Task<CreatedIdRsDto> Update(int id, EmployeeUpdateRqDto employeeUpdateRqDto);

    Task Delete(int id);
}