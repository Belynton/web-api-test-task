CREATE SCHEMA if not exists public;

CREATE TABLE if not exists public.department
(
    id
          SERIAL
        PRIMARY
            KEY,
    name
          character
              varying(20)       NOT NULL,
    phone character varying(12) NOT NULL
);

CREATE TABLE if not exists public.passport
(
    id
           SERIAL
        PRIMARY
            KEY,
    type
           character
               varying(15)       NOT NULL,
    number character varying(12) NOT NULL
);

CREATE TABLE if not exists public.employee
(
    id
                  SERIAL
        PRIMARY
            KEY,
    name
                  character
                      varying(20)       NOT NULL,
    surname       character varying(20) NOT NULL,
    phone         character varying(12) NOT NULL,
    company_id    integer               NOT NULL,
    passport_id   SERIAL                NOT NULL,
    department_id SERIAL                NOT NULL,
    CONSTRAINT employee_fk FOREIGN KEY (department_id) REFERENCES public.department (id),
    CONSTRAINT employee_fk2 FOREIGN KEY (passport_id) REFERENCES public.passport (id)
);

INSERT INTO department(name, phone)
SELECT 'Пензенский',
       '+76044054235'
WHERE NOT EXISTS(
        SELECT id FROM department WHERE id = 1
    );

INSERT INTO department(name, phone)
SELECT 'Московский',
       '+76067354765'
WHERE NOT EXISTS(
        SELECT id FROM department WHERE id = 2
    );

INSERT INTO passport(type, number)
SELECT 'Заграничный',
       '552215'
WHERE NOT EXISTS(
        SELECT id FROM passport WHERE id = 1
    );
INSERT INTO passport(type, number)
SELECT 'Обычный',
       '642355'
WHERE NOT EXISTS(
        SELECT id FROM passport WHERE id = 2
    );

INSERT INTO employee(name, surname, phone, company_id, passport_id, department_id)
SELECT 'Марк',
       'Парамонов',
       '+72274324215',
       1,
       1,
       1
WHERE NOT EXISTS(
        SELECT id FROM employee WHERE id = 1
    );
INSERT INTO employee(name, surname, phone, company_id, passport_id, department_id)
SELECT 'Иван',
       'Петров',
       '+72263454115',
       1,
       2,
       2
WHERE NOT EXISTS(
        SELECT id FROM employee WHERE id = 2
    );