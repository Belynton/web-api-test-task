﻿using Microsoft.AspNetCore.Mvc;
using MyApplication.dto;
using MyApplication.Repository;

namespace MyApplication.Controller;

[Route("api/[controller]/")]
[ApiController]
public class EmployeeController : ControllerBase
{
    private readonly IEmployeeRepository _employeeRepository;

    public EmployeeController(IEmployeeRepository employeeRepository)
    {
        _employeeRepository = employeeRepository;
    }


    // GET api/<EmployeeController>/department/5
    [HttpGet("department/{departmentName}")]
    public async Task<IActionResult> GetWithDepartmentName(string departmentName)
    {
        var employees = await _employeeRepository.FindAllWhereDepartmentName(departmentName);
        return Ok(employees);
    }

    // GET api/<EmployeeController>/company/5
    [HttpGet("company/{companyId}")]
    public async Task<IActionResult> GetWithCompanyId(int companyId)
    {
        var employees = await _employeeRepository.FindAllWhereCompanyId(companyId);
        return Ok(employees);
    }

    // POST api/<EmployeeController>
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] EmployeeCreateRqDto employeeRqDto)
    {
        var result = await _employeeRepository.Create(employeeRqDto);
        return Ok(result);
    }

    // PUT api/<EmployeeController>/5
    [HttpPut("{id}")]
    public async Task<IActionResult> Update(int id, [FromBody] EmployeeUpdateRqDto employeeUpdateRqDto)
    {
        var result = await _employeeRepository.Update(id, employeeUpdateRqDto);
        return Ok(result);
    }

    // DELETE api/<EmployeeController>/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _employeeRepository.Delete(id);
        return Accepted(202);
    }
}